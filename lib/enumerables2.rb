require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.length > 0 ? arr.reduce(:+) : 0
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? do |string|
    sub_tring?(string, substring)
  end
end

def sub_tring?(string, substring)
  string.include?(substring)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  result = []
  string.split(" ").each do |word|
    word.each_char do |letter|
    if string.count(letter) > 1
      result << letter
    end
   end
  end
  result.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split(" ")
  result = [words[0]]
  words.each do |word|
    if word.length > result[0].length
      result << word
    end
    if result.length > 2
      result.shift(1)
    end
  end
  result
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)

  alph = "abcdefghijklmnopqrstuvwxyz"

  new_arr = string.split("")
  new_arr.each do |letter|
    alph.delete!(letter)
  end
  alph.split("")
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  result = []
  (first_yr..last_yr).to_a.each do |year|
    if not_repeat_year?(year)
      result << year
    end
  end
  result
end

def not_repeat_year?(year)
  year.to_s.split("").to_a.uniq.length == year.to_s.split("").to_a.length
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  unique_songs = songs.uniq
  unique_songs.select do |song|
    no_repeats?(song, songs)
  end
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, idx|
    if song == song_name
      return false if song == songs[idx+1]
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  available_words = string.split(" ").select {|word| word.downcase.include?("c")}
  return "" if available_words.length == 0
  available_words.map { |word| remove_punc(word) }
  available_words.sort_by {|x| c_distance(x)}[0]
end

def c_distance(word)
  word.reverse.index("c")
end

def remove_punc(word)
  punc = "?!:,.:;\'\""
  word.delete!(punc)
  word
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = []
  temp_arr = []
     arr.each_with_index do |i, idx|

      if i == arr[idx+1] && i != arr[idx-1]
        temp_arr << idx

      end
      if i != arr[idx+1] && i == arr[idx-1]
        temp_arr << idx
        result << temp_arr
        temp_arr = []
     end
   end 
  result
end
